var validator = require('validator');
module.exports = {

	validateMandatory : function(keysArray, dataObj, errObj) {

		var keyLen = keysArray.length;
		var foundError = false;

		for (var count = 0; count < keyLen; count++) {
			var keyName = keysArray[count];
			if (dataObj[keyName] === undefined || dataObj[keyName] == ""
					|| dataObj[keyName] == null) {
				errObj[keyName] = sails.config.constants.validationError.enterValue;
				foundError = true;
			}
		}
		
		return foundError;

	},

	validateNumeric : function(keysArray, dataObj, errObj) {
		var keyLen = keysArray.length;
		var foundError = false;
		for (var count = 0; count < keyLen; count++) {
			var keyName = keysArray[count];
			if (dataObj[keyName] !== undefined && dataObj[keyName] != ""
					&& dataObj[keyName] != null) {
				if (!validator.isInt(dataObj[keyName])) {
					errObj[keyName] = sails.config.constants.validationError.invalidValue;
					foundError = true;
				}
			}
		}
		
		return foundError;
	},

	validateEmailId : function(keysArray, dataObj, errObj) {
	var keyLen = keysArray.length;
		var foundError = false;
		for (var count = 0; count < keyLen; count++) {
			var keyName = keysArray[count];
			if (dataObj[keyName] !== undefined && dataObj[keyName] != ""
					&& dataObj[keyName] != null) {
				if (!validator.isEmail(dataObj[keyName])) {
					errObj[keyName] = sails.config.constants.validationError.invalidValue;
					foundError = true;
				}
			}
		}
		return foundError;

	},

	validateAlpha : function(keysArray, dataObj, errObj) {
	var keyLen = keysArray.length;
		var foundError = false;
		for (var count = 0; count < keyLen; count++) {
			var keyName = keysArray[count];
			if (dataObj[keyName] !== undefined && dataObj[keyName] != ""
					&& dataObj[keyName] != null) {
				if (!validator.isAlpha((dataObj[keyName]).trim())) {
					errObj[keyName] = sails.config.constants.validationError.invalidValue;
					var foundError = true;
				}
			}
		}
		return foundError;

	},
	
	validateAlphaNumeric : function(keysArray, dataObj, errObj) {
	var keyLen = keysArray.length;
		var foundError = false;
		for (var count = 0; count < keyLen; count++) {
			var keyName = keysArray[count];
			if (dataObj[keyName] !== undefined && dataObj[keyName] != ""
					&& dataObj[keyName] != null) {
				if (!validator.isAlphanumeric((dataObj[keyName]).trim())) {
					errObj[keyName] = sails.config.constants.validationError.invalidValue;
					var foundError = true;
				}
			}
		}
		return foundError;

	},
	
	validateMobileNumber : function(keysArray, dataObj, errObj) {
	var keyLen = keysArray.length;
	var foundError = false;
	var keyLen = keysArray.length;
	var regexExp = /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;
           for (var count = 0; count < keyLen; count++) {
			var keyName = keysArray[count];
			if (dataObj[keyName] !== undefined && dataObj[keyName] != ""
					&& dataObj[keyName] != null) {
				if (!regexExp.test(dataObj[keyName])) {
					errObj[keyName] = "invalid_value";
					foundError = true;
				}
			}
		}
           return foundError;
	},
	
	
	validateEnumeration : function(keysArray, dataObj, errObj,validValues) {
           for (var count = 0; count < keyLen; count++) {
			var keyName = keysArray[count];
			if (dataObj[keyName] !== undefined && dataObj[keyName] != ""
					&& dataObj[keyName] != null) {
				if (!validator.isIn(dataObj[keyName]),validValues) {
					errObj[keyName] = "invalid_value";
				}
			}
		}
           return foundError;
	}

};