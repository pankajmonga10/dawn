module.exports = {
		generateErrorValidationResponse : function(inputObj)
		{
			/*
			 * generate a validation exception response 
			 * appending the input o 
			 */
			var data = {
					rsBody : {
						exceptionBlock :{
							msg : {
								validationException : inputObj
							}
						}
					}
			};
			
			
			return data; 
			
		},
		
		generateBusinessErrorResponse : function(err)
		{
			
			var data = {
					rsBody : {
						exceptionBlock :{
							msg : {
								businessException : err
							}
						}
					}
			};
			
			return data; 
			
		},
		
		generateTechnicalErrorResponse : function(err)
		{
			/*
			 * log error
			 */
			
			return {};
		},
		
		
		generateSuccessResponse : function(inputObj)
		{
			var data = {
					rsBody : inputObj
					};
			
			
		  return data;
		}
}